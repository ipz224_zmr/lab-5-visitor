## DRY (Don't Repeat Yourself)

The code demonstrates the DRY principle by avoiding code duplication. For example, the validation of user input for integers and dates is encapsulated in the [ReadIntegerFromConsole](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ConsoleSOLID/Program.cs?ref_type=heads#L21) and [ValidateAndSetInput](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ClassLibrarySOLID/WarehouseItem.cs?ref_type=heads#L48) methods, respectively. These methods are reused throughout the program to ensure consistent input handling.

## KISS (Keep It Simple, Stupid)

The code follows the KISS principle by maintaining simplicity and readability. The structure of the `Main` method and the usage of clear method names such as [Menu](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ConsoleSOLID/Program.cs?ref_type=heads#L8), [ReadIntegerFromConsole](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ConsoleSOLID/Program.cs?ref_type=heads#L21), and [GetWarehouseItemFromUserInput](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ClassLibrarySOLID/WarehouseItem.cs?ref_type=heads#L67) contribute to its simplicity and ease of understanding.

## SOLID Principles

### Single Responsibility Principle (SRP)

Classes such as [Warehouse](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ClassLibrarySOLID/Warehouse.cs?ref_type=heads), [Reporting](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ClassLibrarySOLID/Reporting%20.cs?ref_type=heads), and [Money](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ClassLibrarySOLID/Money.cs?ref_type=heads) adhere to the SRP by having clear and distinct responsibilities. For example, the `Warehouse` class manages warehouse items, the `Reporting` class handles reporting tasks, and the `Money` class manages monetary calculations.

### Open/Closed Principle (OCP)

The code supports extension without modification, adhering to the OCP. For example, new functionality can be added by extending existing classes or implementing new interfaces without altering the core logic.

### Liskov Substitution Principle (LSP)

The code demonstrates LSP by enabling polymorphic behavior through inheritance. For instance, the [Product](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ClassLibrarySOLID/Product.cs?ref_type=heads) class can be substituted with its derived classes without affecting the behavior of the client code.

### Interface Segregation Principle (ISP)

The [IReporting](https://gitlab.com/ipz224_zmr/design-patterns/-/blob/main/lab%201/ClassLibrarySOLID/IReporting.cs?ref_type=heads) interface adheres to ISP by defining specific methods related to reporting tasks. It ensures that implementing classes only need to provide implementations for methods relevant to reporting, preventing them from being forced to implement unnecessary methods.


## YAGNI (You Aren't Gonna Need It)

The code avoids unnecessary complexity and features that aren't immediately needed. It only implements functionality required for warehouse management, such as adding, removing items, and generating reports, without adding unnecessary features.

## Composition Over Inheritance

The code favors composition over inheritance in several places. For example, the `Warehouse` class encapsulates a list of `WarehouseItem` objects rather than inheriting from a generic list, promoting better code organization and flexibility.

## Program to Interfaces, Not Implementations

The code interacts with objects through interfaces rather than concrete implementations. For example, the `Reporting` class depends on the `IReporting` interface, allowing for interchangeable reporting implementations.

## Fail Fast

The code employs fail-fast behavior by validating user input and handling errors promptly. For instance, invalid input during user interaction results in immediate feedback, guiding users to correct their input.
