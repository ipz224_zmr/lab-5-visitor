﻿
using ComposerClassLibrary;
using ComposerClassLibrary.Visitor;

class Program
{
    static void Main(string[] args)
    {
        var rootElement = new LightElementNode("div", "block", "closing", new List<string> { "class1", "class2" }, new List<LightNode>{
            new LightTextNode("Hello"),
            new LightElementNode("span", "inline", "closing", new List<string>{"class3"}, new List<LightNode>{
                new LightTextNode("World!")
            })
        });
        Console.WriteLine(rootElement.OuterHTML);
        var visitor = new NodePrintingVisitor();

        rootElement.Accept(visitor);
    }
}