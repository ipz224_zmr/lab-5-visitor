﻿using ComposerClassLibrary.Visitor;

namespace ComposerClassLibrary
{
    public class LightNode
    {
        public virtual string OuterHTML { get; }
        public virtual string InnerHTML { get; }

        public virtual void Accept(INodeVisitor visitor)
        {
            throw new NotImplementedException("Accept method should be implemented in subclasses");
        }
    }

}