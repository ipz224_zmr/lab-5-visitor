﻿using ComposerClassLibrary.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary
{

    public class LightTextNode : LightNode
    {
   
        public string Text { get; }

        public LightTextNode(string text)
        {
            Text = text;
        }
        public override string OuterHTML => Text;
        public override string InnerHTML => Text;

        public override void Accept(INodeVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
    

}
