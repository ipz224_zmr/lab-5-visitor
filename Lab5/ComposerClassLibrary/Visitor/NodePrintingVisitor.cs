﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary.Visitor
{
    public class NodePrintingVisitor : INodeVisitor
    {
        public void Visit(LightTextNode node)
        {
            Console.WriteLine($"Text Node: {node.Text}");
        }

        public void Visit(LightElementNode node)
        {
            Console.WriteLine($"Element Node: {node.TagName}");
            foreach (var childNode in node.ChildNodes)
            {
                childNode.Accept(this); 
            }
        }
    }



}
