﻿using ComposerClassLibrary.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComposerClassLibrary
{
    public class LightElementNode : LightNode
    {
        public string TagName { get; }
        public string DisplayType { get; }
        public string ClosingType { get; }
        public List<string> CSSClasses { get; }
        public List<LightNode> ChildNodes { get; }

        public LightElementNode(string tagName, string displayType, string closingType, List<string> cssClasses, List<LightNode> childNodes)
        {
            TagName = tagName;
            DisplayType = displayType;
            ClosingType = closingType;
            CSSClasses = cssClasses;
            ChildNodes = childNodes;
        }

        public override string OuterHTML
        {
            get
            {
                string cssClassStr = string.Join(" ", CSSClasses);
                string attributes = $"class=\"{cssClassStr}\"";
                string childrenHTML = string.Join("", ChildNodes.ConvertAll(node => node.OuterHTML));

                if (ClosingType == "selfClosing")
                    return $"<{TagName} {attributes}/>";
                else
                    return $"<{TagName} {attributes}>{childrenHTML}</{TagName}>";
            }
        }

        public override string InnerHTML
        {
            get
            {
                return string.Join("", ChildNodes.ConvertAll(node => node.OuterHTML));
            }
        }
        public override void Accept(INodeVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
