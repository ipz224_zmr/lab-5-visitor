﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SingletonLibrary;
namespace ConsoleSingleton
{
    class Program
    {
        static void Main()
        {

            Authenticator auth1 = Authenticator.Instance;
            Authenticator auth2 = Authenticator.Instance;
            Authenticator extendedAuth = ExtendedAuthenticator.GetInstance();
            Console.WriteLine($"Порівння екземплярів  класа: {auth1 == auth2}");
            Console.WriteLine($"Порівння екземпляра  класа та класа , що наслідує його: {extendedAuth == auth1}");
            Console.ReadLine();
        }
    }
}
