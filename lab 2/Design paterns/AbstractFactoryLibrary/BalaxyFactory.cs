﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryLibrary
{

    public class BalaxyFactory : IDeviceFactory
    {
        public string Manufacturer => "Balaxy";

        public Laptop CreateLaptop()
        {
            return new Laptop(Manufacturer, "GTX 1660", "Intel Core i5", "14 inch", 8);
        }

        public Netbook CreateNetbook()
        {
            return new Netbook(Manufacturer, "BalaxyModel", "BalaxyProcessor", 8, 256);
        }

        public EBook CreateEBook()
        {
            return new EBook(Manufacturer, "6 inch");
        }

        public Smartphone CreateSmartphone()
        {
            return new Smartphone(Manufacturer, "Galaxy S20", "Android", "6.2 inch");
        }
    }
}
