﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryLibrary
{
    public class EBook : Device
    {
        public string ScreenSize { get; }
        public EBook(string manufacturer, string screenSize) : base(manufacturer)
        {

            this.ScreenSize = screenSize;
        }
        public override string ToString()
        {
            return base.ToString() + $"Screen Size: {this.ScreenSize}";
        }
    }
}
