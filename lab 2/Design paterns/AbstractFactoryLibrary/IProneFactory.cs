﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryLibrary
{
    public class IProneFactory : IDeviceFactory
    {
        public string Manufacturer => "IProne";

        public Laptop CreateLaptop()
        {
            return new Laptop(Manufacturer, "RTX 3060 TI", "I7-13990F", "32 inch", 16);
        }

        public Netbook CreateNetbook()
        {
            return new Netbook(Manufacturer, "IProneModel", "IProneProcessor", 8, 256);
        }

        public EBook CreateEBook()
        {
            return new EBook(Manufacturer, "7 inch");
        }

        public Smartphone CreateSmartphone()
        {
            return new Smartphone(Manufacturer, "IProneModel", "IOS", "6 inch");
        }
    }

}
