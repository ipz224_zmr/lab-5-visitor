﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryMethodLibrary;

namespace ConsoleFabricMetod
{

    public class Program
    {
        public static void Main(string[] args)
        {

            ISubscriptionFactory factory = new WebSite();
            Subscription websiteSubscription = factory.CreateSubscription();

            factory = new MobileApp();
            Subscription mobileAppSubscription = factory.CreateSubscription();

            factory = new ManagerCall();
            Subscription managerCallSubscription = factory.CreateSubscription();

            websiteSubscription.PrintSubscriptionInfo();
            mobileAppSubscription.PrintSubscriptionInfo();
            managerCallSubscription.PrintSubscriptionInfo();
            Console.ReadLine();
        }
    }
}