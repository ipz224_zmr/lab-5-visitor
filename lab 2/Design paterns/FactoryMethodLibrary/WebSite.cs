﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodLibrary
{
    public class WebSite : ISubscriptionFactory
    {
        public Subscription CreateSubscription()
        {

            return new DomesticSubscription(new List<string> { "General", "News", "Entertainment" });
        }
    }
}
