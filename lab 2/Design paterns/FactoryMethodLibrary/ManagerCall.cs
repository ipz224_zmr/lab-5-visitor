﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodLibrary
{
    public class ManagerCall : ISubscriptionFactory
    {
        public Subscription CreateSubscription()
        {
            return new PremiumSubscription(new List<string> { "General", "News", "Entertainment", "Educational", "Documentary", "Science", "Sports", "Movies" });
        }
    }
}
