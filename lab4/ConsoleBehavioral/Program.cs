﻿


using ChainOfResponsClassLibrary;

class Program
{
    static void Main(string[] args)
    {

        var problemLevel = new FirstLevel();

        problemLevel.SetfollowingHandler(new SecondLevel())
            .SetfollowingHandler(new ThirdLevel())
            .SetfollowingHandler(new FourthLevel())
            .SetfollowingHandler(new FifthLevel());

        var problems = new List<string> { "Пристрій повільно працює",
            "Збій у роботі програм", 
            "Пристрій часто перезавантажується", 
            "Пристрій не працює",
            "Пристрій автоматично змінює громкість",
            "Проблема з мережею",
            "Невідома проблема" };


        problems.ForEach(problem =>
        {
            Console.WriteLine(problem);
            problemLevel.Inquiry(new UserInquiry { UserProblem = problem });
        });

    }
}