﻿using StrategyClassLibrary;
using static System.Net.Mime.MediaTypeNames;

class Program
{
    static void Main(string[] args)
    {
        IImageLoadingStrategy loadingStrategy = new NetworkImageLoadingStrategy();
        var image = new StrategyClassLibrary.Image(loadingStrategy, "https://www.thesprucepets.com/thmb/sG_y-M_7pT_4nEbuTX-pLb-70Ow=/750x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/GettyImages-174770333-0f52afc06a024c478fafb1280c1f491f.jpg");

        image.DisplayImage();
        Console.WriteLine($"{image.OuterHTML}");
        
        Console.WriteLine("\n\n\n\n");
        loadingStrategy = new FilesystemImageLoadingStrategy();
        StrategyClassLibrary.Image image2 = new StrategyClassLibrary.Image(loadingStrategy, "C:\\Users\\Maks\\Desktop\\лаби\\4 семестр\\Конструювання програмного забезпечення (2022-2023)\\design-patterns\\lab 4\\cat.jpg");
        image2.DisplayImage();
        Console.WriteLine($"{image2.OuterHTML}");

    }
}
