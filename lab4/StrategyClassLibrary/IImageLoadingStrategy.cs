﻿namespace StrategyClassLibrary
{
    public interface IImageLoadingStrategy
    {
        byte[] LoadImage(string href);
    }
}