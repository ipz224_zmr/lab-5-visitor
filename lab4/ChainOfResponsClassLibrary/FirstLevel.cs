﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsClassLibrary
{
    public class FirstLevel : Handler
    {
        protected override string Report => "Спробуйте завантажити оновлення системи(якщо наявне) та перезавантажити телефон";

        protected override int HelpLevel => 1;

        protected override string HandableProblem => "Збій у роботі програм";

        public override void Inquiry(UserInquiry inquiry)
        {
            if (inquiry.UserProblem.Equals(this.HandableProblem, StringComparison.CurrentCultureIgnoreCase))
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"Рівень {this.HelpLevel} підтримка: {this.Report}");
                Console.ResetColor();
                return;
            }

            base.Inquiry(inquiry);
        }
    }
}
