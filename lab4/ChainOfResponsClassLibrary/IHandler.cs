﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsClassLibrary
{
   public interface IHandler
    {
        void Inquiry(UserInquiry inquiry);
    }
}
