﻿using ComposerClassLibrary;

namespace LightweightClassLibrary
{
    public class HTMLConverter
    {
        public  static string GetTextFromFile(string filePath)
        {

            if (!File.Exists(filePath))
            {
                Console.WriteLine($"Файл '{filePath}' не знайдено.");
                return null;
            }


            try
            {

                return File.ReadAllText(filePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Помилка при зчитуванні файлу: {ex.Message}");
                return null;
            }

        }
        // Метод без легковаговика
        public static ComposerClassLibrary.LightElementNode ConvertTextToHTML(string text)
        {

            string[] lines = string.IsNullOrWhiteSpace(text) ? new string[0] : text.Split('\n');

            var rootNode = new ComposerClassLibrary.LightElementNode("div", "block", "normal", new List<string>(), new List<LightNode>());

            rootNode.ChildNodes.Add(new ComposerClassLibrary.LightElementNode("h1", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode($"{lines[0].Trim()} \n") }));

            for (int i = 1; i < lines.Length; i++)
            {
                string line = lines[i].TrimEnd();
                LightNode node;

                if (string.IsNullOrWhiteSpace(line))
                {
                    node = new ComposerClassLibrary.LightElementNode("p", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode("<br>  \n") });
                }
                else if (char.IsWhiteSpace(line[0]))
                {
                    node = new ComposerClassLibrary.LightElementNode("blockquote", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode($" {line} \n") });
                }
                else if (line.Length < 20)
                {
                    node = new ComposerClassLibrary.LightElementNode("h2", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode($" {line} \n") });
                }
                else
                {
                    node = new ComposerClassLibrary.LightElementNode("p", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode($"{line} \n") });
                }

                rootNode.ChildNodes.Add(node);
            }

            return rootNode;
        }


        // Метод з використанням легковаговика
        public static LightElementNode ConvertTextToHTMLWithLW(string text)
        {
            string[] lines = string.IsNullOrWhiteSpace(text) ? new string[0] : text.Split('\n');
            var rootNode = LightElementNode.GetElementNode("div", "block", "normal", new List<string>(), new List<LightNode>());

            rootNode.ChildNodes.Add(LightElementNode.GetElementNode("h1", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode($"{lines[0].Trim()} \n") }));

            for (int i = 1; i < lines.Length; i++)
            {
                string line = lines[i].TrimEnd();
                LightNode node;

                if (string.IsNullOrWhiteSpace(line))
                {
                    node = LightElementNode.GetElementNode("p", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode("<br>  \n") });
                }
                else if (char.IsWhiteSpace(line[0]))
                {
                    node = LightElementNode.GetElementNode("blockquote", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode($" {line} \n") });
                }
                else if (line.Length < 20)
                {
                    node = LightElementNode.GetElementNode("h2", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode($" {line} \n") });
                }
                else
                {
                    node = LightElementNode.GetElementNode("p", "block", "normal", new List<string>(), new List<LightNode> { new LightTextNode($"{line} \n") });
                }

                rootNode.ChildNodes.Add(node);
            }

            return rootNode;
        }
    }
}
