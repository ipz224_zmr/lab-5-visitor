﻿using ComposerClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightweightClassLibrary
{
    public static class CalculateMemory
    {
        public static long CalculateMemoryUsage(LightNode node)
        {
            long size = 0;

            size += System.GC.GetTotalMemory(false);

            size += CalculateNodeMemoryUsage(node);

            return size;
        }

        public static long CalculateNodeMemoryUsage(LightNode node)
        {
            long size = 0;

            if (node is LightTextNode textNode)
            {
                size += sizeof(char) * textNode.OuterHTML.Length;
            }
            else if (node is LightElementNode elementNode)
            {
                size += sizeof(char) * elementNode.OuterHTML.Length;
                foreach (var child in elementNode.ChildNodes)
                {
                    size += CalculateNodeMemoryUsage(child);
                }
            }

            return size;
        }
    }
}
