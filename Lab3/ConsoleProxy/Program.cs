﻿using ProxyClassLibrary;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
    
        string filePath1 = "file1.txt";
        string filePath2 = "file2.txt";

     
        File.WriteAllText(filePath1, "Hello\nWorld!");
        File.WriteAllText(filePath2, "World\nHello!\n Thanks!");

        ITextReader reader1 = new SmartTextChecker();
        char[][] result1 = reader1.ReadText(filePath1);
        char[][] result3 = reader1.ReadText(filePath2);
        Console.WriteLine("//////////////////////////////////////////////////////////////");

        ITextReader reader2 = new SmartTextReaderLocker("file1.*");

        char[][] result2 = reader2.ReadText(filePath1);
        char[][] result4 = reader2.ReadText(filePath2);



        File.Delete(filePath1);
        File.Delete(filePath2);
    }
}