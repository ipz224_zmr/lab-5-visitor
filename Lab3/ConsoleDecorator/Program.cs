﻿using static System.Net.Mime.MediaTypeNames;
using System.Threading;
using DecoratorClassLibrary;

class Program
{
    static void Main(string[] args)
    {

  

        Character warrior = new Warrior("Warrior", new ArtifactsInventory(new WeaponsInventory(new СlothesInventory())));
        Console.WriteLine(warrior.Inventory.Equip("Warrior"));
        warrior.Attack();

     
        Character mage = new Mage("Mage", new WeaponsInventory(new ArtifactsInventory(new СlothesInventory())));
        Console.WriteLine(mage.Inventory.Equip("Mage"));
        mage.Attack();

        Character paladin = new Paladin("Paladin", new СlothesInventory(new WeaponsInventory(new ArtifactsInventory())));
        Console.WriteLine(paladin.Inventory.Equip("Paladin"));
        paladin.Attack();
    }
} 