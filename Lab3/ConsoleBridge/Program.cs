﻿using BridgeClassLibrary;

class Program
{
    static void Main(string[] args)
    {
      
        IRenderer rasterRenderer = new RasterDrawing();

    
        IRenderer vectorRenderer = new VectorDrawing();

        
        Shape circle = new Circle(rasterRenderer);
        circle.Draw();

        Shape square = new Square(vectorRenderer);
        square.Draw();

        Shape triangle = new Triangle(rasterRenderer);
        triangle.Draw();
    }
}